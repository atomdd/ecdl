#pragma once
#include <type_traits>
#include <array>
#include <stdio.h>

template<int bins,class T>
class dynamic_histogram{
    static_assert(bins>0,"bin count has to be positive");
    static_assert(std::is_integral_v<T>,"histogram type has to be integer");
    private:
    std::array<std::make_unsigned_t<T>,2*bins+1> data;
    T base;
    int count;
    bool error;
    public:
    dynamic_histogram(){
        for(int i=0;i<(2*bins+1);i++)
            data[i]=0;
        count=0;
        error=false;
    }
    void add(T in){
        //if it's 1st element
        if(count==0){
            base=in;
            data.at(bins)=1;
        } else {
            int offset=in-base;
            if(offset>=bins || offset<=-bins){
                error=true;
            } else {
                data.at(bins+offset)++;
            }
        }
        count++;
    }
    void print(){
        printf("%s\n",error ? "histogram failed" : "histogram succeded");
        for(int i=0;i<(2*bins+1);i++)
            printf("%x: %d\n",base-bins+i,data[i]);
    }
};