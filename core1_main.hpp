#pragma once
#include <stdio.h>

#include "pico/stdlib.h"

#include "hw_drivers/TH_lib.hpp"
#include "hw_drivers/LD_lib.hpp"
#include "hw_drivers/ADC_lib.hpp"
#include "hw_drivers/TEC_lib.hpp"
#include "hw_drivers/FLASH_write_sync.hpp"
#include "config/io_state.hpp"

float error_integral=0.0f;

void core1_main(void){
    IO_state io_state = {.dt=0.1, .TEC_power = {0,0}};
    int counter=0;
    while(1){
        //download current enviroment data
        io_state.TH_temp[0] = TH1.get_temp();
        io_state.TH_temp[1] = TH2.get_temp();
        io_state.LD_power = LD_singleton.get_current() * ADC_singleton.get_LD();
        //run control loop
        //drive TECs with current settings
        TEC1.set_power(io_state.TEC_power[0]);
        TEC2.set_power(io_state.TEC_power[1]);
        sleep_ms(100);
    }
}