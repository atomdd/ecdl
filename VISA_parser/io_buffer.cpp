#include "io_buffer.hpp"
#include <string_view>
#include <algorithm>

void io_buffer_class::reset()
{pointer=0;}

void io_buffer_class::add_char(char in){
    // handling of stupid cr/lf/crlf problem
    // microsoft introduced 40 years ago...
    // we simply convert both to \n
    // this'll generate empty line for \r\n
    // that's ignored by remover of empty lines
    if(in=='\r')
            in='\n';
    //if backspace or del remove last char from ring
    //del is sent by picocom after pressind backspace for some reason
    if(in==8 || in==127){
        if(pointer>0){
            pointer--;
            return;
        }
    }
    if(in=='\n' && pointer==0)
        return;
    internal_buffer.at(pointer)=in;
    pointer++;
}

std::string_view io_buffer_class::to_string_view(){
    return std::string_view(internal_buffer.begin(),pointer);
}

bool io_buffer_class::is_line_ready(){
    return std::find(internal_buffer.begin(),internal_buffer.begin()+pointer,'\n')!=internal_buffer.begin()+pointer;
}