#include "configuration_variable.hpp"
#include "parser.hpp"
#include "state_machine.hpp"
#include <iostream>
#include <cstring>

/*void *operator new(std::size_t n) 
{     
    std::cout << "heap allocation!\n";
    std::exit(3);
}*/

state_machine global_state_machine;

int main()
{
    parser* global_parser=global_state_machine.generate_parser();
    memcpy(const_cast<FLASH_storage*>(&config_file),&flash_copy,sizeof(FLASH_storage));
    lexer test_tokens;

    global_parser->print_help(0);
    while(1)
    {
        std::string input;
        getline(std::cin,input);
        input+='\n';
        test_tokens.parse_line(input);
        global_parser->parse(test_tokens,0);
    }
    print_config();
    return 0;
}