#pragma once
#include <string_view>
#include <functional>
#include <array>
#include <cctype>
#include <algorithm>
#include <charconv>
#include "print_string_view.hpp"

void my_assert(bool in)
{
    if(!in)
        std::exit(1);
}

void my_assert(bool in,std::string_view comment)
{
    if(!in){
        print_string_view(comment);
        putchar('\n');
        std::exit(1);
    }
}

class lexer{
    public:
    const static unsigned int MAX_TOKEN_COUNT=10;
    std::array<std::string_view,MAX_TOKEN_COUNT> tokens;
    unsigned int instructions;
    unsigned int params;
    bool query;
    void parse_line(std::string_view line){
        enum class halfs{
            INSTRUCTION,
            DATA
        } which_half=halfs::INSTRUCTION;
        //my_assert that it's a line
        my_assert(std::string_view::npos != line.find('\n'),"not a line");
        //my_assert that it's a single line
        my_assert(line.rfind('\n') == line.find('\n'),"too many lines");
        //is it query
        query= (std::string_view::npos != line.find('?'));
        unsigned int current,i=0;
        instructions=0;
        while(std::string_view::npos != line.find_first_of(" :,")){
            //to ignore spaces and colons in parameters
            if(which_half==halfs::INSTRUCTION)
                current=line.find_first_of(" :");
            else
                current=line.find_first_of(",");
            //if we found space we switch to DATA mode
            if(current==line.find_first_of(" ")){
                which_half=halfs::DATA;
                instructions=i+1;
            }
            tokens.at(i)=line.substr(0,current);
            line=line.substr(current+1);
            //trim multiple spaces
            line=line.substr(line.find_first_not_of(' '));
            //std::cout<<tokens[i]<<" : "<<line<<std::endl;
            i++;
        }
        //parse last token
        tokens.at(i)=line.substr(0,line.find('\n'));
        //set token counters appropriately
        if(instructions==0){
            instructions=i+1;
            params=0;
        }
        else
            params=i-instructions+1;
    }
    void print_tokens_all(){
        printf("is query:");
        putchar(query);
        printf(" instructions: ");
        print_type(instructions);
        printf(" params: ");
        print_type(params);
        putchar('\n');
        for(int i=0;i<MAX_TOKEN_COUNT;i++){
            print_string_view(tokens[i]);
            putchar(' ');
        }
        putchar('\n');
    }
};

class parser{
    public:
    virtual void const parse(lexer&,unsigned int) = 0;
    virtual void print_help(int) const = 0;
    virtual ~parser(){};
};

template<size_t count>
class level_parser : public parser{
    private:
    std::array<std::string_view,count> tokens;
    std::array<const parser*,count> callbacks;
    std::function<void(lexer&,unsigned int)> default_callback;

    //matches if input is valid token as described by token input
    //that is if input is uppercas epart of token or lowercase part of otken
    bool match(const std::string_view token,std::string_view input){
        unsigned int obligatory=std::find_if(token.begin(), token.end(), [](char c) { return std::islower(c); })-token.begin();
        std::string_view upper=token.substr(0,obligatory);
        //if there's ? at the end
        if(input.rfind('?')!=std::string_view::npos)
        input=input.substr(0,input.size()-1);
        if(input.size()==upper.size()){
            for(unsigned int i=0;i<upper.size();i++){
                if(tolower(token[i])!=tolower(input[i]))
                    return false;
            }
            return true;
        }
        if(input.size()==token.size()){
            for(unsigned int i=0;i<upper.size();i++){
                if(tolower(token[i])!=tolower(input[i]))
                    return false;
            }
            return true;
        }
        return false;
    }
    private:
    const std::array<std::string_view,count> static extract_first(std::array<std::pair<std::string_view,const parser*>,count> tokens_and_callbacks){
        std::array<std::string_view,count> tokens;
            for(unsigned int i=0;i<count;i++)
                tokens[i]=tokens_and_callbacks[i].first;
            return tokens;
    }
    const std::array<const parser*,count> static extract_second(std::array<std::pair<std::string_view,const parser*>,count> tokens_and_callbacks){
        std::array<const parser*,count> callbacks;
            for(unsigned int i=0;i<count;i++)
                callbacks[i]=tokens_and_callbacks[i].second;
            return callbacks;
    }
    public:
    void const parse(lexer& input,unsigned int depth){
        for(unsigned int i=0;i<count;i++){
            //TODO: it's just debug, remove
            // printf("matching: ");
            // print_string_view(tokens[i]);
            // printf(" | ");
            // print_string_view(input.tokens[depth]);
            // printf(" : ");
            // putchar(match(tokens[i],input.tokens[depth]));
            // putchar('\n');
            if(match(tokens[i],input.tokens[depth])){
                const_cast<parser*>(callbacks[i])->parse(input,depth+1);
                return;
            }
        }
        default_callback(input,depth+1);
    }
    void print_help(int depth) const{
        putchar('\n');
        for(unsigned int i=0;i<count;i++){
            for(unsigned int j=0;j<depth;j++)
                printf("   ");
            printf("|->");
            print_string_view(tokens[i]);
            callbacks[i]->print_help(depth+1);
        }
    }
    level_parser(const std::array<std::pair<std::string_view,const parser*>,count> tokens_and_callbacks,
                 const std::function<void(lexer&,unsigned int)> default_callback):
    default_callback(default_callback),
    tokens(extract_first(tokens_and_callbacks)),
    callbacks(extract_second(tokens_and_callbacks))
    {}
    level_parser()
    {}
    level_parser<count>(level_parser<count>&&) = default;
    level_parser<count>(const level_parser<count>&) = default;
    level_parser<count>& operator= (const level_parser<count>& a){
        this->callbacks=a.callbacks;
        this->default_callback=a.default_callback;
        this->tokens=a.tokens;
        return *this;
    }
};

class function_parser : public parser{
    private:
    std::function<void(lexer&,unsigned int)> callback;
    std::string_view help;
    public:
    void const parse(lexer& input,unsigned int depth)
    {callback(input,depth);}
    void print_help(int) const{
        printf(" : ");
        print_string_view(help);
        putchar('\n');
    }
    function_parser& operator= (const function_parser& a)=default;
    function_parser(const std::function<void(lexer&,unsigned int)> callback,const std::string_view help)
    :callback(callback),help(help)
    {}
    function_parser()
    {}
};

template<class T>
void from_chars(std::basic_string_view<char>::const_iterator first,std::basic_string_view<char>::const_iterator last,T& ptr){
    std::from_chars(first,last,ptr);
}

template<>
void from_chars(std::basic_string_view<char>::const_iterator first,std::basic_string_view<char>::const_iterator last,bool& ptr){
    std::string_view text(first,last-first);
    if(text=="true"){
        ptr=true;
        return;
    }
    if(text=="false"){
        ptr=false;
        return;
    }
    if(text=="1"){
        ptr=true;
        return;
    }
    if(text=="0"){
        ptr=false;
        return;
    }
    my_assert(true,"bool parser failed");
}

template<class T>
class variable_parser : public parser{
    private:
    volatile T* ptr;
    std::string_view msg;
    std::string_view help;
    public:
    variable_parser<T>(volatile T* const ptr,const std::string_view msg,const std::string_view help):
    ptr(ptr),msg(msg),help(help)
    {}

    void const parse(lexer& input,unsigned int depth){
        my_assert(input.instructions<=depth,"too deep");
        if(input.query){
            print_string_view(msg);
            printf(" = ");
            print_type<T>(*ptr);
            putchar('\n');
        }
        else{
        my_assert(input.params>0,"no param");
        my_assert(input.params==1,"too many params");
            from_chars(input.tokens[depth].begin(),input.tokens[depth].end(),*const_cast<T*>(ptr));
        }
    }
    void print_help(int) const {
        printf(" : set/get ");
        print_string_view(help);
        printf(" variable\n");}
    variable_parser<T>& operator= (const variable_parser<T>& a)=default;
    variable_parser<T>(){}
};

class readonly_parser : public parser{
    private:
    std::function<void(void)> callback;
    std::string_view help;
    public:
    readonly_parser(const std::function<void(void)> callback,const std::string_view help)
    :callback(callback),help(help)
    {}

    void const parse(lexer& input,unsigned int depth){
        my_assert(input.instructions<=depth,"too deep");
        if(input.query){
            callback();
        }
        else{
            printf("this is readonly parameter\n");
        }
    }
    void print_help(int) const {
    printf(" : get ");
    print_string_view(help);
    printf("\n");}
    readonly_parser& operator= (readonly_parser&&)=default;
    readonly_parser& operator= (const readonly_parser &a)=default;
    readonly_parser(){}
};

