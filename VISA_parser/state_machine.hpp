#pragma once
#include "parser.hpp"
#include "configuration_variable.hpp"
#include "hw_drivers/ADC_lib.hpp"
#include "hw_drivers/TEC_lib.hpp"
#include "hw_drivers/LD_lib.hpp"
#include "hw_drivers/TH_lib.hpp"
//control loops
#include "control_loops/autotuning/autotune_procedure.hpp"
#include "control_loops/control_loops_api.hpp"

constexpr bool static inline case_insensitive_comparator(std::string_view,const char*);
class state_machine{
    private:
    enum class MODE{
        MANUAL,
        AUTOMATIC,
    } mode;
    level_parser<11> root;
    level_parser<6> TEC_config[2];
    level_parser<9>  TH_config[2];
    level_parser<5>  LD_config;
    readonly_parser ADC_config;
    level_parser<0> CTRL_loop_config;
    const std::function<void(lexer&,unsigned int)> default_callback[2];
    void wrong_instruction_callback(unsigned int depth){
        printf("your instruction is wrong at level: %d\n",depth+1);
    }
    void set_get_TEC_power(lexer& lex,int channel){
        if(lex.query){
            if(channel==0)
                printf("TEC1 power is %f\n",TEC1.get_power());
            else
                printf("TEC2 power is %f\n",TEC2.get_power());
        }
        else
            if(mode==MODE::MANUAL){
                my_assert(lex.params>0,"no param");
                my_assert(lex.params==1,"too many params");
                float tmp;
                std::from_chars(lex.tokens[lex.instructions].begin(),lex.tokens[lex.instructions].end(),tmp);
                if(channel==0)
                    TEC1.set_voltage(tmp);
                else
                    TEC2.set_voltage(tmp);
            }
            else
                printf("error!!! can't set TEC power in auto mode\n");
    }
    variable_parser<float> maxV_callback[2];
    variable_parser<float> Res_callback[2];
    variable_parser<float> Rth_callback[2];
    variable_parser<bool> heating_callback[2];
    variable_parser<bool> invert_callback[2];
    function_parser power_callback[2];
    const level_parser<6> TEC_generator(unsigned int channel){
        maxV_callback[channel] =variable_parser(&config_file.TEC[channel].max_voltage,"maximum voltage","maximum voltage");
        Res_callback[channel]  =variable_parser(&config_file.TEC[channel].max_voltage,"resistance","resistance");
        Rth_callback[channel]  =variable_parser(&config_file.TEC[channel].max_voltage,"Rth","Rth");
        heating_callback[channel]  =variable_parser(&config_file.TEC[channel].heating,"justheating","just heating (true if heater not TEC)");
        invert_callback[channel]  =variable_parser(&config_file.TEC[channel].invert,"invert","invert output");
        power_callback[channel]=function_parser([channel,this](lexer& lex,unsigned int){set_get_TEC_power(lex,channel);},"current TEC power");
        return level_parser<6>(
            {std::make_pair("MAXVoltage",dynamic_cast<const parser*>(&maxV_callback[channel])),
            std::make_pair("RESistance",dynamic_cast<const parser*>(&Res_callback[channel])),
            std::make_pair("RTHermal",dynamic_cast<const parser*>(&Rth_callback[channel])),
            std::make_pair("Power",dynamic_cast<const parser*>(&power_callback[channel])),
            std::make_pair("HEATing",dynamic_cast<const parser*>(&heating_callback[channel])),
            std::make_pair("INVert",dynamic_cast<const parser*>(&invert_callback[channel]))},
            default_callback[1]
        );
    }
    variable_parser<float> A_callback[2];
    variable_parser<float> B_callback[2];
    variable_parser<float> C_callback[2];
    variable_parser<float> D_callback[2];
    variable_parser<float> offset_callback[2];
    variable_parser<float> gain_callback[2];
    readonly_parser TH_query_V[2];
    readonly_parser TH_query_R[2];
    readonly_parser TH_query_T[2];
    void mode_control(lexer& lex,unsigned int depth){
        if(lex.query){
            switch(mode)
            {
                case MODE::MANUAL:
                    printf("manual mode\n");
                break;
                default:
                    //TODO: uncomment this is just for refactor
                    //printf("current mode is: %s\n",control_loop_name());
                break;
            }
        }
        else {
            if(lex.params!=1){
                printf("wrong amount of parameters!\n");
                return;
            }
            if(case_insensitive_comparator(lex.tokens[depth],"MANUAL"))
            {
                printf("switched to manual mode\n");
                mode=MODE::MANUAL;
            }
            //TODO: uncomment this is just for refactor
            // else if(control_loop_factory(lex.tokens[depth]))
            //     mode=MODE::AUTOMATIC;
            else
                printf("error! unknown mode\n");
        }
    }
    template<int channel>
    const level_parser<9> TH_generator(){
        A_callback[channel] =variable_parser(&config_file.TH[channel].A,"A coefficient","A coefficient");
        B_callback[channel] =variable_parser(&config_file.TH[channel].B,"B coefficient","B coefficient");
        C_callback[channel] =variable_parser(&config_file.TH[channel].C,"C coefficient","C coefficient");
        D_callback[channel] =variable_parser(&config_file.TH[channel].D,"D coefficient","D coefficient");
        offset_callback[channel] =variable_parser(&config_file.TH[channel].offset,"op-amp offset","op-amp offset");
        gain_callback[channel] =variable_parser(&config_file.TH[channel].gain,"op-amp gain","op-amp gain");
        TH_query_V[channel] =readonly_parser([](){printf("TH%d voltage     is %f\n",channel,get_correct_channel<channel>()->get_temp());},"query raw ADC voltage");
        TH_query_R[channel] =readonly_parser([](){printf("TH%d resistance  is %f\n",channel,get_correct_channel<channel>()->get_resistance());},"query thermistor resistance");
        TH_query_T[channel] =readonly_parser([](){printf("TH%d temperature is %f\n",channel,get_correct_channel<channel>()->get_temp());},"query temperature");
        return level_parser<9>(
            {std::make_pair("Acoefficient",dynamic_cast<const parser*>(&A_callback[channel])),
             std::make_pair("Bcoefficient",dynamic_cast<const parser*>(&B_callback[channel])),
             std::make_pair("Ccoefficient",dynamic_cast<const parser*>(&C_callback[channel])),
             std::make_pair("Dcoefficient",dynamic_cast<const parser*>(&D_callback[channel])),
            std::make_pair("OFFSET",dynamic_cast<const parser*>(&offset_callback[channel])),
            std::make_pair("GAIN",dynamic_cast<const parser*>(&gain_callback[channel])),
            std::make_pair("Voltage",dynamic_cast<const parser*>(&TH_query_V[channel])),
            std::make_pair("Resistance",dynamic_cast<const parser*>(&TH_query_R[channel])),
            std::make_pair("Temperature",dynamic_cast<const parser*>(&TH_query_T[channel]))},
            default_callback[1]
        );
    }
    void set_get_LD_current(lexer& lex){
        if(lex.query)
            printf("LD current is %f\n",LD_singleton.get_current());
        else
            if(mode==MODE::MANUAL){
                float tmp;
                std::from_chars(lex.tokens[lex.instructions].begin(),lex.tokens[lex.instructions].end(),tmp);
                printf("setting LD current to: %f\n",tmp);
                LD_singleton.set_current(tmp);
            }
            else
                printf("error!!! can't set LD current in auto mode\n");
    }
    variable_parser<float> LD_Imax_config;
    variable_parser<float> LD_Ithreshold_config;
    variable_parser<float> LD_Umax_config;
    variable_parser<float> LD_Umin_config;
    function_parser LD_query_current;
    void fill_LD_parser(){
        LD_Imax_config=variable_parser(&config_file.LD.max_current,"maximum laser diode current","maximum laser diode current");
        LD_Ithreshold_config=variable_parser(&config_file.LD.max_current,"minimum laser diode current","minimum laser diode current");
        LD_Umax_config=variable_parser(&config_file.LD.max_voltage,"maximum laser diode voltage","maximum laser diode voltage");
        LD_Umin_config=variable_parser(&config_file.LD.min_voltage,"minimum laser diode voltage","minimum laser diode voltage");
        LD_query_current=function_parser([this](lexer& lex,unsigned int){set_get_LD_current(lex);},"laser diode current");
        LD_config=level_parser<5>(
        {std::make_pair("Imax",dynamic_cast<const parser*>(&LD_Imax_config)),
        std::make_pair("Ithreshold",dynamic_cast<const parser*>(&LD_Ithreshold_config)),
        std::make_pair("Umax",dynamic_cast<const parser*>(&LD_Umax_config)),
        std::make_pair("Umin",dynamic_cast<const parser*>(&LD_Umin_config)),
        std::make_pair("Icurrent",dynamic_cast<const parser*>(&LD_query_current))},
        default_callback[1]
        );
    }
    void fill_CTRL_parser()
    {}
    function_parser mode_control_callback;
    static constexpr auto dump_lambda=[](void)->void{print_config(config_file);};
    readonly_parser dump_callback;
    function_parser help_callback;
    function_parser save_callback;
    void fill_root_parser(){
        mode_control_callback=function_parser([this](lexer& a,unsigned int b){this->mode_control(a,b);},"allows to get/set current mode");
        dump_callback=readonly_parser(dump_lambda,"print config variables");
        help_callback=function_parser([this](lexer&,unsigned int){root.print_help(0);},"print instruction set");
        save_callback=function_parser([this](lexer&,unsigned int){save_to_flash();},"saves settings to flash");
        root=level_parser<11>(
        {std::make_pair("TEC1",dynamic_cast<const parser*>(&TEC_config[0])),
        std::make_pair("TEC2",dynamic_cast<const parser*>(&TEC_config[1])),
         std::make_pair("LDiode",dynamic_cast<const parser*>(&LD_config)),
         std::make_pair("TH1",dynamic_cast<const parser*>(&TH_config[0])),
         std::make_pair("TH2",dynamic_cast<const parser*>(&TH_config[1])),
         std::make_pair("ADC",dynamic_cast<const parser*>(&ADC_config)),
         std::make_pair("MODE",dynamic_cast<const parser*>(&mode_control_callback)),
         std::make_pair("DUMP",dynamic_cast<const parser*>(&dump_callback)),
         std::make_pair("HELP",dynamic_cast<const parser*>(&help_callback)),
         std::make_pair("SAVE",dynamic_cast<const parser*>(&save_callback)),
         std::make_pair("CTRLloop",dynamic_cast<const parser*>(&CTRL_loop_config))},
        default_callback[0]
    );
    }
    public:
    parser* generate_parser(){
        fill_LD_parser();
        fill_CTRL_parser();
        fill_root_parser();
        TEC_config[0]=TEC_generator(0);
        TEC_config[1]=TEC_generator(1);
        TH_config[0]=TH_generator<0>();
        TH_config[1]=TH_generator<1>();
        ADC_config=readonly_parser([](){printf("VCC: %f LD: %f\n",ADC_singleton.get_VCC(),ADC_singleton.get_LD());},"read state of ADCs");
        return &root;
    }
    state_machine()
    : default_callback{
        [this](lexer&,unsigned int depth)->void {wrong_instruction_callback(0);},
        [this](lexer&,unsigned int depth)->void {wrong_instruction_callback(1);}}
    {}
};

constexpr bool static inline case_insensitive_comparator(std::string_view a,const char* b){
    auto ptr=a.begin();
    while(ptr!=a.end() && b!=0){
        if(tolower(*ptr)!=tolower(*b))
            return false;
        ptr++;
        b++;
    }
    if(ptr==a.end() && *b==0)
        return true;
    else
        return false;
}