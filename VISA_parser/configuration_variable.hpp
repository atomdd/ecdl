#pragma once
#include "stdint.h"
#include <cstdio>
#include <cstring>
#include "hardware/flash.h"
#include "hardware/sync.h"
#include "pico/platform.h"
#include "hw_drivers/FLASH_write_sync.hpp"
#include "config/flash.hpp"


/* 
the base configuration is that positive values of TEC power/voltage should cool corresponding thermistor.
All adaptatvie algorithms don't mind but manual control, manual tuning and PID tuning relay on this relation
*/
namespace{
    const inline FLASH_storage flash_copy  __attribute__ ((aligned (4096)))=
    {   .powerup_counter=0,
        .TH={
            {.offset=0,.gain=2,.A=2.69071099e-03,.B=2.72436433e-04,.C=8.94440628e-06,.D=-9.35852511e-07},
            {.offset=0,.gain=2,.A=2.69071099e-03,.B=2.72436433e-04,.C=8.94440628e-06,.D=-9.35852511e-07}},
        .TEC={
            {.max_voltage=4.0f, .resistance=2,.invert=1,.heating=0}, //dTmax=62C, Qcmax=4.4W
            {.max_voltage=12.0f,.resistance=3,.invert=1,.heating=0}},//dTmax=60C, Qcmax=27W
        .ADC={.vcc_divider=0.20518,.ld_divider=0.20518},
        .LD={.threshold_current=0.001,.max_current=0.075,.min_voltage=4.0,.max_voltage=6.3,.current_to_voltage=50,.offset=0},
        .monitor={.UVLO=5.5f},
        .slew_limiter={.TEC_slew={{0.1,true},{0.1,true}},.LD_slew={0,false}},
        .PID={{0,0},{0,0},{0,0},{0,0}}
    };
    const inline uint8_t* flash_ptr=reinterpret_cast<uint8_t*>(const_cast<FLASH_storage*>(&flash_copy))+0x03000000;
}

volatile inline FLASH_storage config_file __attribute__ ((aligned (256)));

static inline void __attribute__((optimize("O0"))) save_to_flash() {
    lock_flash();
    printf("erasing\n");
    uint32_t ints = save_and_disable_interrupts();
    printf("interrupts saved\n");
    constexpr size_t program_size=256*(sizeof(FLASH_storage)/256) + (sizeof(FLASH_storage)%256 ? 256 : 0);
    static_assert(program_size>=sizeof(FLASH_storage),"program size lower than config file. rounding math is wrong");
    flash_range_erase(reinterpret_cast<uint32_t>(&flash_copy)-XIP_BASE,4096);
    flash_range_program(reinterpret_cast<uint32_t>(&flash_copy)-XIP_BASE,const_cast<uint8_t*>(config_file.padding),program_size);
    restore_interrupts(ints);
    printf("written\n");
    unlock_flash();
}

static inline void load_from_flash(){
    printf("copying from: %p\n",flash_ptr);
    memcpy(const_cast<uint8_t*>(config_file.padding),flash_ptr,4096);
}