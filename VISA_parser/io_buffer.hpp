#pragma once
#include <string_view>
#include <array>

class io_buffer_class{
    private:
    std::array<char,256> internal_buffer;
    int pointer=0;
    public:
    void reset();
    void add_char(char);
    std::string_view to_string_view();
    bool is_line_ready();
};

inline io_buffer_class io_buffer;