import numpy as np
import matplotlib.pyplot as plt

#dt  - time step
#Cp  - heat capacity
#Pin - external power input
#Rth - thermal resistance between components
#Tam - ambient temperature
#H   - convention cooling
#pump- power pumped by TEC

#only ambient cooling of outer baseplate
#0.1-1 W/K from math
H=np.array([0,0,5]) #W/K
Tamb=25
Cp=np.array([1/14.2,1/50.5,1/363]) #K/J
#dt=10**-3 # 1 ms
dt=1

class TEC:
    '''class describing model of TEC'''
    def __init__(self,Pelectricalmax,Pthermalmax,Tmax):
        self.max_temp =Tmax
        self.max_power=Pelectricalmax
        self.max_thermal=Pthermalmax
        self.thermal_resistance=2.5*Pthermalmax/Tmax
    def transfered(self,Pin):
        return np.copysign(self.max_thermal/self.max_power*np.sqrt(np.abs(Pin*self.max_power))/(1+np.abs(Pin)/self.max_power),Pin)

class control_loop:
    state=np.array([0,0],dtype=float)
    def proces(self,inp):
        self.state+=inp*np.array([10**-2,10**-2])
        return self.state+np.array([inp[0]*0.1,inp[1]*1])

PLD=0.3
PTEC1=0.3
PTEC2=1

T_set=np.array([15,20])
PID=control_loop()
TEC1=TEC(2*3.75,4.4 ,67)
TEC2=TEC(4*15.4,33.4,66)
_Rtec1=TEC1.thermal_resistance #W/K
_Rtec2=TEC2.thermal_resistance #W/K
Rth=np.array([[-_Rtec1,_Rtec1,0],[_Rtec1,-_Rtec1-_Rtec2,_Rtec2],[0,_Rtec2,-_Rtec2]]) # W/K

T=np.ones(3)*Tamb #start with ambient temps
T_acc=[]

for _ in range(3000):
    TEC1_tr=TEC1.transfered(PTEC1)
    TEC2_tr=TEC2.transfered(PTEC2)
    Pin=np.array([PLD,np.abs(PTEC1),np.abs(PTEC2)]) #W
    pump=np.array([-TEC1_tr,TEC1_tr-TEC2_tr,TEC2_tr]) #W
    T+= dt * Cp * (Pin + Rth@T - H * (T-Tamb) + pump)
    error=T[0:2]-T_set
    [PTEC1,PTEC2]=PID.proces(error)
    T_acc.append(np.copy(T))

print('TEC power',PTEC1,PTEC2)
T_acc=np.array(T_acc)
plot_start=2000
plt.plot(T_acc[plot_start:,0]-T_set[0],label='LD')
plt.plot(T_acc[plot_start:,1]-T_set[1],label='inner')
#plt.plot(T_acc[plot_start:,2],label='external')
plt.title('temps of elements')
plt.legend()
plt.show()
plot_start=0
T_acc=np.diff(T_acc,axis=0)
plt.plot(T_acc[plot_start:,0],label='LD')
plt.plot(T_acc[plot_start:,1],label='inner')
plt.plot(T_acc[plot_start:,2],label='external')
plt.title('energy transfers')
plt.legend()
#plt.show()


#TEC specs:
#|->small ones:
#|   |-> TES1-03102 - 0R8-1R , Imax=2A, 3V75, Tmax>=67C, Qcmax=5W  
#|   |-> TES1-03104 - 0R8-1R , Imax=4A, 3V75, Tmax>=67C, Qcmax=9W  
#|->big ones:
#|   |-> TES1-12704 - 2R8-3R2, Imax=4A, 15V4, Tmax>=66C, Qcmax=33W4