#include <stdio.h>
#include <string_view>
#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "hardware/adc.h"

#include "VISA_parser/state_machine.hpp"
#include "VISA_parser/configuration_variable.hpp"
#include "VISA_parser/io_buffer.hpp"
#include "hw_drivers/TH_lib.hpp"
#include "hw_drivers/TEC_lib.hpp"
#include "hw_drivers/ADC_lib.hpp"
#include "hw_drivers/LD_lib.hpp"
#include "monitor_lib.hpp"
#include "core1_main.hpp"
#include "hw_drivers/FLASH_write_sync.hpp"

state_machine global_state_machine;
lexer lex;

extern "C" void _getentropy(){}
int main()
{ 
    stdio_init_all();
    while(!stdio_usb_connected());
    printf("init\n");
    parser* global_parser=global_state_machine.generate_parser();
    TEC1.init();
    TEC2.init();
    TEC1.sync_channels();
    ADC_singleton.init();
    TH1.init();
    TH2.init();
    LD_singleton.init_spi();
    sleep_ms(500); //time needed for ADC to fill buffers
    TEC1.set_voltage(5,12);
    TEC1.set_voltage(5.001,12);
    printf("VCC: %f\n",ADC_singleton.get_VCC());
    printf("LD:  %f\n",ADC_singleton.get_LD());
    printf("TH states: %d\t%d\n",TH1.get_code(),TH2.get_code());
    load_from_flash();
    TEC1.enable();
    TEC2.enable();
    config_file.powerup_counter++;
    printf("power up counter: %d\n",config_file.powerup_counter);
    multicore_launch_core1(core1_main);
    save_to_flash();
    
    while(1)
    {
        //timeout 0 get's stuck for some reason
        int read=getchar_timeout_us(10);
        //int read=getchar();
        //add character if there's any
        if((read!=PICO_ERROR_TIMEOUT)&&(read!=0))
        {
            DEBUG(2,"got char");
            io_buffer.add_char(read);
            //local echo
            if(read==127 || read==8){
                //move cursor back
                putchar(8);
                //remove last char
                putchar(' ');
                //move cursor back again
                putchar(8);
            } else if (read=='\r'){
                putchar('\n');
            } else {
                putchar(read);
            }
            DEBUG(2,"added char");
            //parse if full line
            if(io_buffer.is_line_ready())
            {
                DEBUG(2,"parsing full line");
                printf("string length: %zu\n",io_buffer.to_string_view().length());
                lex.parse_line(io_buffer.to_string_view());
                DEBUG(3,"tokenized");
                global_parser->parse(lex,0);
                DEBUG(3,"dispatched");
                io_buffer.reset();
            }
        }
        monitor.main_loop_protector();
        yield_if_needed();
    }
}