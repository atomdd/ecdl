#pragma once
#include "hw_drivers/ADC_lib.hpp"
#include "hw_drivers/LD_lib.hpp"
#include "hw_drivers/TEC_lib.hpp"
#include "VISA_parser/configuration_variable.hpp"
#include <cmath>

class monitor_sketch{
    private:
    float last_Vcc=std::nan("");
    bool undervoltage_reported=false;
    //additional limit on 5.2V just in case someone made a software bug
    bool disable_power_vcc_comparator(float Vcc){
        return ((Vcc<config_file.monitor.UVLO) || (Vcc<5.2f));
    }
    public:
    void main_loop_protector(){
        //protection of USB from TECs turning on
        float Vcc=ADC_singleton.get_VCC();
        //init state
        if(std::isnan(last_Vcc) || (disable_power_vcc_comparator(Vcc) ^ disable_power_vcc_comparator(last_Vcc))){
            if(disable_power_vcc_comparator(Vcc)){
            TEC1.disable();
            TEC2.disable();
            DEBUG(1,"disabing TECs");
            }
            else{
                //if(autoenable)
                TEC1.enable();
                TEC2.enable();
                DEBUG(1,"enabling TECs");
            }
        }
        last_Vcc=Vcc;
        //protection of LD
        float LD=ADC_singleton.get_LD();
        if(LD>config_file.LD.max_voltage){
            DEBUG(1,"LD overvoltage");
            LD_singleton.disable();
        }
        if(LD<config_file.LD.min_voltage && LD_singleton.get_current()>config_file.LD.threshold_current){
            if(!undervoltage_reported){
                LOG("LD undervoltage");
                undervoltage_reported=true;
            }
        }
        else {
            undervoltage_reported=false;
        }
        //saturated op-amp can't regulate LD's current
        //and may generate spike when VCC rapidly changes
        float MOS_voltage=ADC_singleton.get_MOS()-LD_singleton.get_resistor_voltage();
        if(MOS_voltage<0.5f){
            if(LD_singleton.blanked)
                DEBUG(1,"disabling LD due to lack of headroom");
            LD_singleton.blanked=false;
        }
        //hysteresis to not oscillate
        if(MOS_voltage>1.0f){
            if(!LD_singleton.blanked)
                DEBUG(1,"enabling LD headroom restored");
            LD_singleton.blanked=true;
        }
    }
};

inline monitor_sketch monitor;