#ifndef LOGGING_GUARD
#define LOGGING_GUARD
//required to be able to compile code with both g++ and arm-gcc for AMD64 and rp2040 respectively
#ifdef PICO_DEFAULT_LED_PIN
#include "pico/stdlib.h"
#endif
#include <stdio.h>

namespace{
    const uint8_t DEBUG_LEVEL=0;
}

static inline void ERROR(const char* msg){
    printf("ERROR: %s\n",msg);
    while(1){}
}

static inline void LOG(const char* comment){
    printf("LOG: %s\n",comment);
}

template<class T>
void LOG(const char* comment,T data){
    printf("can't log %s, missing type specialization\n",comment);
}

static inline void DEBUG(uint8_t level,const char* comment){
    if(level<=DEBUG_LEVEL)
        printf("DEBUG%d %s\n",level,comment);
}

static inline void DEBUG(uint8_t level,int comment){
    if(level<=DEBUG_LEVEL)
        printf("DEBUG%d %d\n",level,comment);
}

template<>
inline void LOG<float>(const char* comment,float data){
    printf("LOG: %s %f\n",comment,data);
}

template<>
inline void LOG<int>(const char* comment,int data){
    printf("LOG: %s %d\n",comment,data);
}

template<>
inline void LOG<int16_t>(const char* comment,int16_t data){
    printf("LOG: %s %d\n",comment,data);
}

template<>
inline void LOG<uint16_t>(const char* comment,uint16_t data){
    printf("LOG: %s %d\n",comment,data);
}

#endif //LOGGING_GUARD