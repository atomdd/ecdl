DMA allocation:
0 - ADC
1,2 - TEC1
3,4 - TEC2
5,6 - TH1&2

PIO allocation:
pio0
|->sm0, offset zero, len 8 (may change in the future) - TH1&2

PWM allocation:
TEC1 -> slice 3
TEC2 -> slice 0