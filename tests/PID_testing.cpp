#include "catch.hpp"
#include "../control_loops/PID.hpp"

using namespace control_loop;

TEST_CASE("PID class")
{
    struct Config
    {
        struct {
            std::array<float,2> P;
            std::array<float,2> I;
            std::array<float,2> D;
            std::array<float,2> FF;
        } PID;
    };
    SECTION("zero doesn't move")
    {
        control_loop::set_temps = {0, 0};
        Config config={
            .PID={
                .P  = {1, 1},
                .I  = {0, 0},
                .D  = {0, 0},
                .FF = {0, 0}
                }
            };
        CLPID<Config> test(config);
        auto res = test.execute(0, {0,0});
        REQUIRE(res == std::array<float, 2>({0.0f, 0.0f}));
        res = test.execute(1, {0,0});
        REQUIRE(res == std::array<float, 2>({0.0f, 0.0f}));
        control_loop::set_temps = {1, 1};
        res = test.execute(0, {1,1});
        REQUIRE(res == std::array<float, 2>({0.0f, 0.0f}));
    }
    SECTION("FF test")
    {
        control_loop::set_temps = {0, 0};
        Config config={
            .PID={
                .P  = {0, 0},
                .I  = {0, 0},
                .D  = {0, 0},
                .FF = {1, 1}
                }
            };
        CLPID<Config> test(config);
        auto res = test.execute(0, {0,0});
        REQUIRE(res == std::array<float, 2>({0.0f, 0.0f}));
        res = test.execute(1, {0,0});
        REQUIRE(res == std::array<float, 2>({0.0f, 0.0f}));
        control_loop::set_temps = {1, 1};
        res = test.execute(0, {1,1});
        REQUIRE(res == std::array<float, 2>({0.0f, 0.0f}));
    }
}