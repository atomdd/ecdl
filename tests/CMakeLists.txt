cmake_minimum_required(VERSION 3.13)
project(tests)

set(CMAKE_CXX_STANDARD 17)

add_executable(test_suite
main_stub.cpp
parser_testing.cpp ../VISA_parser/io_buffer.cpp
exp_fit_testing.cpp ../expo_fit/exp_eval.hpp
PID_testing.cpp ../control_loops/PID.hpp
)