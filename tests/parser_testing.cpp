#include "catch.hpp"
#include "../VISA_parser/io_buffer.hpp"
#include "../VISA_parser/parser.hpp"

TEST_CASE("io_buffer")
{
    io_buffer_class DUT;
    SECTION("init state")
    {
        REQUIRE(DUT.is_line_ready() == false);
        REQUIRE(DUT.to_string_view() == "");
    }
    SECTION("adding characters")
    {
        DUT.add_char('a');
        REQUIRE(DUT.is_line_ready() == false);
        REQUIRE(DUT.to_string_view() == "a");
        DUT.add_char('b');
        REQUIRE(DUT.is_line_ready() == false);
        REQUIRE(DUT.to_string_view() == "ab");
        DUT.add_char('c');
        REQUIRE(DUT.is_line_ready() == false);
        REQUIRE(DUT.to_string_view() == "abc");
    }
    SECTION("readyness test")
    {
        DUT.add_char('a');
        DUT.add_char('b');
        DUT.add_char('c');
        SECTION("simple \\n test")
        {
            REQUIRE(DUT.is_line_ready() == false);
            DUT.add_char('\n');
            REQUIRE(DUT.is_line_ready() == true);
        }
        SECTION("simple \\r test")
        {
            REQUIRE(DUT.is_line_ready() == false);
            DUT.add_char('\r');
            REQUIRE(DUT.is_line_ready() == true);
        }
        SECTION("not be ready after reset")
        {
            REQUIRE(DUT.is_line_ready() == false);
            DUT.add_char('\r');
            DUT.reset();
            REQUIRE(DUT.is_line_ready() == false);
        }
    }
    SECTION("backspace test")
    {
        DUT.add_char('a');
        DUT.add_char('b');
        DUT.add_char('c');
        DUT.add_char(8);
        REQUIRE(DUT.to_string_view() == "ab");
    }
    SECTION("capacity test")
    {
        bool failed=0;
        for(int i=0;i<256;i++)
            REQUIRE_NOTHROW(DUT.add_char('a'));
        REQUIRE_THROWS(DUT.add_char('a'));
    }
}

TEST_CASE("parser")
{
    SECTION("lexer testing")
    {
        lexer DUT;
        SECTION("white char SCPI")
        {
            //due to my_assert we can't test on it
            // DUT.parse_line("");
            // REQUIRE(DUT.instructions == 0);
            // REQUIRE(DUT.params == 0);
            // REQUIRE(DUT.query == 0);
            DUT.parse_line("\n");
            REQUIRE(DUT.instructions == 1);
            REQUIRE(DUT.params == 0);
            REQUIRE(DUT.query == 0);
            //due to my_assert we can't test on it
            // DUT.parse_line(" ");
            // REQUIRE(DUT.instructions == 0);
            // REQUIRE(DUT.params == 0);
            // REQUIRE(DUT.query == 0);
            DUT.parse_line(" \n");
            REQUIRE(DUT.instructions == 1);
            REQUIRE(DUT.params == 1);
            REQUIRE(DUT.query == 0);
        }
        SECTION("valid SCPI")
        {
            DUT.parse_line("a:b c,d\n");
            REQUIRE(DUT.instructions == 2);
            REQUIRE(DUT.params == 2);
            REQUIRE(DUT.query == 0);
            DUT.parse_line("a:b? c,d\n");
            REQUIRE(DUT.instructions == 2);
            REQUIRE(DUT.params == 2);
            REQUIRE(DUT.query == 1);
            DUT.parse_line("a:b    c,d\n");
            REQUIRE(DUT.instructions == 2);
            REQUIRE(DUT.params == 2);
            REQUIRE(DUT.query == 0);
        }
    }
}