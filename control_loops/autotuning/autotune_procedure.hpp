#include "hw_drivers/FLASH_write_sync.hpp"
#include "hw_drivers/TH_lib.hpp"
#include "hw_drivers/TEC_lib.hpp"
#include "logging.hpp"

#include "flexible_time_series.hpp"

namespace{
    const unsigned int LOOP_DURATION=90; //in seconds
    const float BASELINE_ERROR=0.03f;
    const float MIN_STEP=1.0f;
    const float POWER_LIMIT=0.3f;
    template<int channel>
    void autotune_single_TEC(const float baseline[]){
        static_assert(channel==0 || channel==1,"wrong channel");
        auto TEC=get_correct_TEC<channel>();
        float max_power=config_file.TEC[channel].max_voltage;
        max_power=max_power*max_power/config_file.TEC[channel].resistance;
        float power=max_power*0.01; //we start from 1% max power
        //searching for minimum power for either TH to step at least MIN_STEP
        while(1){
            //wait for settle down
            DEBUG(2,"waiting to return to baseline");
            while(true){
                if(std::fabs(TH1.get_temp()-baseline[0])<BASELINE_ERROR)
                    if(std::fabs(TH2.get_temp()-baseline[1])<BASELINE_ERROR)
                        break;
            }
            //trying to excite enough change
            DEBUG(2,"enabling TEC");
            printf("testing %f power\n",power);
            TEC->set_power(power);
            for(int i=0;i<(LOOP_DURATION*20);i++){
                if(std::fabs(TH1.get_temp()-baseline[0])>MIN_STEP){
                    DEBUG(2,"TH1 exeeded the threshold");
                    goto after_loop;
                }
                if(std::fabs(TH2.get_temp()-baseline[0])>MIN_STEP){
                    DEBUG(2,"TH2 exeeded the threshold");
                    goto after_loop;
                }
                sleep_ms(50);
            }
            printf("temps changed by: %2.2f W %2.2f W\n",TH1.get_temp()-baseline[0],TH2.get_temp()-baseline[0]);
            //cooling down again
            TEC->set_power(0);
            power*=2;
            if(power>max_power*POWER_LIMIT){
                LOG("unable to find safe power limit to trigger required step");
                LOG("consider increasing time limit, power limit or improve design");
            }
        }
        after_loop:
        //cool down Peltier after measurement. Otherwise the other channel will never work!
        TEC->set_power(0);
        printf("power needed to excite %f change in temp on %d TEC is %f\n",MIN_STEP,1+channel,power);
    }
}

void autotuning_function(){
    float baseline[2];
    LOG("ENTERED AUTOTUNER. VISA WILL NOT RESPOND UNTIL THE END");
    lock_flash();
    baseline[0]=get_correct_channel<0>()->get_temp();
    baseline[1]=get_correct_channel<1>()->get_temp();
    DEBUG(3,"baseline collected");
    autotune_single_TEC<0>(baseline);
    autotune_single_TEC<1>(baseline);
    unlock_flash();
}