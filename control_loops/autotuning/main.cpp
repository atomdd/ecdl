#include "flexible_time_series.hpp"
#include <iostream>
#include <ostream>

template<unsigned int N>
void print(const flexible_time_series<N>& in){
    std::cout<<"internals: ";
    for(int i=0;i<in.data_size();i++)
        std::cout<<in[i]<<' ';
    std::cout<<std::endl;
}

template<unsigned int N>
void print_all(const flexible_time_series<N>& in){
    print(in);
    //std::cout<<"arr size "<<(in.data_size())<<"\t raw size "<<(in.raw_size())<<std::endl;
}


int main(){
    flexible_time_series<10> test;
    for(int i=0;i<42;i++)
    {
        print_all(test);
        test.push_back(i);
    }
    return 0;
}