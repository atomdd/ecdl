#pragma once
#include <stdint.h>
#include <iostream>

#include "../../logging.hpp"
namespace{
    const bool LOCAL_DEBUG=true;
}

template<unsigned int buffer_length>
class flexible_time_series{
    private:
    static_assert(buffer_length%2==0);
    static_assert(buffer_length<65536); //we need it smaller so our internal counter doesn't overflow
    uint16_t buffer[buffer_length];
    uint16_t divider; //so we can store at most buffer_length*2**15 data points, should be plenty
    uint16_t internal_counter,external_counter;
    public:
    void reset(){
        divider=1;
        internal_counter=0;
        external_counter=0;
    }
    void push_back(uint16_t data){
        //std::cout<<"[push back pre ] "<<"int "<<internal_counter<<" ext "<<external_counter<<" div "<<divider<<std::endl;
        if(external_counter==0){
            //relocation time
            if(internal_counter==buffer_length){
                for(int i=2;i<buffer_length;i+=2)
                    buffer[i/2]=buffer[i];
                //point past last shifted point
                internal_counter=buffer_length/2;
                //increase divider
                divider=divider<<1;
                if constexpr (LOCAL_DEBUG)
                {
                    for(int i=buffer_length/2;i<buffer_length;i++)
                        buffer[i]=0;
                }
            }
            buffer[internal_counter]=data;
            internal_counter++;
        }
        external_counter++;
        if(external_counter==divider)
            external_counter=0;
    }
    unsigned int data_size() const{
        return internal_counter;
    }
    unsigned int raw_size() const{
        int tmp=external_counter;
        if(tmp==0)
            tmp=divider;
        return (internal_counter-1)*divider+tmp;
    }
    uint16_t operator[] (unsigned int i) const{
        if(i>buffer_length)
            ERROR("OUT OF BOUND ACCESS in flexible time series");
        return buffer[i];
    }
    flexible_time_series<buffer_length>()
    {reset();}
};