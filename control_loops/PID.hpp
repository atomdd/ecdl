#pragma once
#include "control_loops_template.hpp"

namespace control_loop
{
    template<class Config>
    class CLPID : public CLtemplate
    {
        public:
        CLPID(const Config &config_file)
        : config_file(config_file)
        {}
        void execute(IO_state&)
        {
            // std::array<float,2> control_words={0,0};
            // const std::array<float,2> errors=control_loop::set_temps-current_temps;
            // const std::array<float,2> error_deltas=current_temps-last_temps;
            // //Proportional
            // control_words+=errors*(*const_cast<std::array<float,2>*>(&config_file.PID.P));
            // //Integral
            // control_words+=integrator*(*const_cast<std::array<float,2>*>(&config_file.PID.I));
            // //Derivative
            // //Feed-Forward

            // //integration
            // integrator+=errors*time_delta;
            // //differentiation
            // return {0,0};
        }
        const char* name()
        {return "PID";}
        private:
            std::array<float,2> integrator={0,0};
            std::array<float,2> last_temps;
            const Config &config_file;
            //the noninitialized differnetiatoe
    };
}