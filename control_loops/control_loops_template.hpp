#pragma once

#include <array>
#include "config/io_state.hpp"

namespace control_loop
{
    class CLtemplate
    {
        public:
        virtual ~CLtemplate() {}
        virtual void execute(IO_state&) = 0;
        const virtual char* name() = 0;
        CLtemplate(){};
        CLtemplate(CLtemplate  &) = delete;
        CLtemplate(CLtemplate &&) = delete;
        CLtemplate operator=(CLtemplate  &) = delete;
        CLtemplate operator=(CLtemplate &&) = delete;
    };
}

// namespace
// {
//     template<class T,long unsigned int N>
//     const std::array<T,N> operator- (const std::array<T,N> A, const std::array<T,N> B)
//     {
//         std::array<T,N> res;
//         for(unsigned int i=0;i<N;i++)
//             res[i]=A[i]-B[i];
//         return res;
//     }
//     template<class T,long unsigned int N>
//     const std::array<T,N> operator+ (const std::array<T,N> A, const std::array<T,N> B)
//     {
//         std::array<T,N> res;
//         for(unsigned int i=0;i<N;i++)
//             res[i]=A[i]+B[i];
//         return res;
//     }
//     template<class T,long unsigned int N>
//     const std::array<T,N> operator* (const std::array<T,N> A, const std::array<T,N> B)
//     {
//         std::array<T,N> res;
//         for(unsigned int i=0;i<N;i++)
//             res[i]=A[i]*B[i];
//         return res;
//     }
//     template<class T,long unsigned int N>
//     const std::array<T,N> operator* (const std::array<T,N> A, const T B)
//     {
//         std::array<T,N> res;
//         for(unsigned int i=0;i<N;i++)
//             res[i]=A[i]*B;
//         return res;
//     }
//     template<class T,long unsigned int N>
//     std::array<T,N>& operator+= (std::array<T,N>& A,std::array<T,N>& B)
//     {
//         for(unsigned int i=0;i<N;i++)
//             A[i]+=B[i];
//         return A;
//     }
//     template<class T,long unsigned int N>
//     std::array<T,N>& operator+= (std::array<T,N>& A,const std::array<T,N>& B)
//     {
//         for(unsigned int i=0;i<N;i++)
//             A[i]+=B[i];
//         return A;
//     }
// }