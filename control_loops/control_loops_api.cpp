#include <cctype>
#include <cmath>
#include <cstdlib>
#include <optional>
#include <string_view>
#include <optional>
#include <variant>

#include "VISA_parser/configuration_variable.hpp"
#include "logging.hpp"
#include "control_loops_template.hpp"
#include "PID.hpp"

namespace
{
    // class{
    // public:
    //     void executor(float time_delta)
    //     {
    //         for(int i=0;i<2;i++)
    //             if(config_file.slew_limiter.TEC_slew[i].enabled)
    //                 control_loop::set_temps[i]=_slew(set_temps[i],control_loop::set_temps[i],
    //                 time_delta*config_file.slew_limiter.TEC_slew[i].slew);
    //             else
    //                 control_loop::set_temps[i]=set_temps[i];
    //         if(config_file.slew_limiter.LD_slew.enabled)
    //             control_loop::set_current=_slew(set_current,control_loop::set_current,
    //             time_delta*config_file.slew_limiter.LD_slew.slew);
    //         else
    //             control_loop::set_current=set_current;
    //     }
    //     void params_set(std::optional<float> temp[2],std::optional<float> LD_current)
    //     {
    //         for(int i=0;i<2;i++)
    //             set_temps[i]=temp[i].value_or(set_temps[i]);
    //         set_current=LD_current.value_or(set_current);
    //     }
    // private:
    //     float _slew(float set, float current, float step)
    //     {
    //         if(set==current)
    //             return set;
    //         float diff=set-current;
    //         if(std::abs(diff)<step)
    //             return set;
    //         diff=std::copysign(step,diff);
    //         return current+diff;
    //     }
    //     float set_temps[2];
    //     float set_current;
    // } slew_limiter;

    constexpr bool case_insensitive_comparator(std::string_view a,const char* b)
    {
        auto ptr=a.begin();
        while(ptr!=a.end() && b!=0){
            if(tolower(*ptr)!=tolower(*b))
                return false;
            ptr++;
            b++;
        }
        if(ptr==a.end() && *b==0)
            return true;
        else
            return false;
    }
}
namespace control_loop
{
    class CLdummy : public CLtemplate
    {
        public:
        CLdummy(){}
        void execute(float time_delta){};
        const char* name()
        {return "impossible!";}
    };
    inline std::variant<CLdummy, CLPID> current_loop;
    CLtemplate *current_loop_ptr=nullptr;
}

void control_loop_executor(IO_state& io_state)
{
    if(control_loop::current_loop_ptr==nullptr)
        ERROR("control loop is empty but shouldn't");
    control_loop::current_loop_ptr->execute(io_state);
}

bool control_loop_factory(std::string_view name)
{
    if(case_insensitive_comparator(name,"PID"))
    {
        printf("initializing PID\n");
        control_loop::current_loop.emplace<control_loop::CLPID>();
        control_loop::current_loop_ptr=&std::get<control_loop::CLPID>(control_loop::current_loop);
    }
    else if(case_insensitive_comparator(name,"TRIPPLE_PID"))
        DEBUG(0,"TRIPPLE_PID initiated");
    else
        return false;
    return true;
}

const char* control_loop_name()
{
    if(control_loop::current_loop_ptr==nullptr)
        ERROR("control loop is empty but shouldn't");
    return control_loop::current_loop_ptr->name();
}
