#pragma once

#include <optional>
#include <string_view>
#include "config/io_state.hpp"

void control_loop_executor(IO_state&);
//true if recognised
bool control_loop_factory(std::string_view name);
const char* control_loop_name();