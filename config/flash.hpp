#pragma once
#include <array>
#include <stdint.h>

#include "control_loop.hpp"

struct TH_params{
    //float offset=3.0,gain=3;
    //float A=2.69071099e-03,B=2.72436433e-04,C=8.94440628e-06,D=-9.35852511e-07;
    float offset,gain;
    float A,B,C,D;
};

struct TEC_params{
    float max_voltage;
    float resistance;
    float thermal_resistance;
    bool invert;
    bool heating;
};

struct ADC_params{
    float vcc_divider;
    float  ld_divider;
};

struct LD_params{
    float threshold_current;
    float max_current;
    float min_voltage;
    float max_voltage;
    float current_to_voltage,offset;
};

struct monitoring_params{
    float UVLO;
};

struct slew_limiter_params{
    struct{
        float slew;
        bool enabled;
    } TEC_slew[2], LD_slew; //K/s
};

union FLASH_storage{
    struct{
        uint32_t powerup_counter; //if zero then it's just after programming
        TH_params TH[2];
        TEC_params TEC[2];
        ADC_params ADC;
        LD_params LD;
        monitoring_params monitor;
        slew_limiter_params slew_limiter;
        struct{
            PID_params PID;
        };
    };
    uint8_t padding[4096];
};

void print_config(volatile FLASH_storage &config_file);