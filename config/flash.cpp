#include <cstdio>
#include "flash.hpp"


void print_config(volatile FLASH_storage &config_file)
{
    printf("\033[1;31m!!!CONFIG DUMP START!!!\033[0m\n");
    printf("I've been on %d times\n",config_file.powerup_counter);
    for(int ch=0;ch<2;ch++)
    {
        printf("thermistor%d config:\n",ch+1);
        printf("|->offset=%f\tgain=%f\n",config_file.TH[ch].offset,config_file.TH[ch].gain);
        printf("|->A,B,C,D=%f,%f,%f,%f\n",config_file.TH[ch].A,config_file.TH[ch].B,config_file.TH[ch].C,config_file.TH[ch].D);
    }
    for(int ch=0;ch<2;ch++)
    {
        printf("Peltier%d config:\n",ch);
        printf("|->max voltage=%f\n",config_file.TEC[ch].max_voltage);
        printf("|->resistance =%f\n",config_file.TEC[ch].resistance);
        printf("|->Rth        =%f\n",config_file.TEC[ch].thermal_resistance);
    }
    printf("ADC config:\n");
    printf("|->vcc divider=%f\n",config_file.ADC.vcc_divider);
    printf("|->LD  divider=%f\n",config_file.ADC.ld_divider);
    printf("LD  config:\n");
    printf("|->threshold current=%f\n",config_file.LD.threshold_current);
    printf("|->maximum   current=%f\n",config_file.LD.max_current);
    printf("|->minimum   voltage=%f\n",config_file.LD.min_voltage);
    printf("|->maximum   voltage=%f\n",config_file.LD.max_voltage);
    printf("monitoring config: (TODO)\n");
    printf("slew limiter config: (TODO)\n");
    printf("CONTROL LOOPS:\n");
    printf("(TODO)\n");
    printf("\033[1;31m!!!CONFIG DUMP END!!!\033[0m\n");
}