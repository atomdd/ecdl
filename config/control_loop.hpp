#pragma once

struct PID_params{
    std::array<float,2> P;
    std::array<float,2> I;
    std::array<float,2> D;
    std::array<float,2> FF;
};

