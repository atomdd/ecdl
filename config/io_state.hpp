#pragma once
#include <array>

struct IO_state{
    std::array<float, 2> TH_temp;
    float LD_power;
    float dt;

    std::array<float, 2> TEC_power;
};