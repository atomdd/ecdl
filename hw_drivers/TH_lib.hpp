#ifndef TH_LIB_GUARD
#define TH_LIB_GUARD
#include <cmath>
#include "dynamic_historam.hpp"
#include "pico/stdlib.h"
#include "hardware/dma.h"
#include "VISA_parser/configuration_variable.hpp"
#include "TH_ADC_comunication.pio.h"
#include "logging.hpp"

namespace{
    //conversion constants
    const inline float REF=5.0f;
    const inline float REF_2=REF/2;
    const inline float RESISTOR=10'000;
    //data aquisition constants
    const unsigned int LOOP_LENGTH_LOG=10;
    const unsigned int LOOP_LENGTH=1<<LOOP_LENGTH_LOG; //at 100kHz that'll have ~5ms of temp data
    //pin definitions
    const int DIN=16;
    const int CLK=18;
    const int TRIGGER=19;
    //state machine constants
    const int _state_machine=0;
    const float CLK_DIVIDER=125.0f/40; //we want to run at roughly 40MHz
    //statics from class. They can't be statics as template makes them into two fully separate instances
    inline uint16_t buffer[LOOP_LENGTH] __attribute__ ((aligned (LOOP_LENGTH*2)));
    inline uint32_t restart_ptr=reinterpret_cast<uint32_t>(&buffer[0]); //despite const we want to keep it in ram
    inline static uint8_t _configured=0;

    static inline float R_to_T(float R,float A,float B,float C,float D){
        float lnR=logf(R/1000.0f); //fit was done on resistance in Kohm
        float tmp=A+lnR*(B+lnR*(C+lnR*D));
        return 1.0f/tmp-273.15f; //convert K->C
    }
    static inline float V_to_R(float V, float gain, float offset){
        //get divider voltage
        V=REF_2+(V-REF_2)/gain-offset;
        float R=RESISTOR*V/(REF-V);
        return R;
    }
    static inline float  code_to_V(uint16_t code){
        float V=(REF*code)/(1<<16);
        return V;
    }
    static inline float  code_to_V(float code){
        float V=(REF*code)/(1<<16);
        return V;
    }
    static inline float code_to_temp(uint16_t code,const TH_params &config){
        return R_to_T(V_to_R(code_to_V(code),config.gain,config.offset),config.A,config.B,config.C,config.D);
    }
    static inline float code_to_temp(float code,const TH_params &config){
        return R_to_T(V_to_R(code_to_V(code),config.gain,config.offset),config.A,config.B,config.C,config.D);
    }

    template<int TH_channel>
    class TH_driver{
        private:
        const static int main_DMA_channel=5,chain_DMA_channel=6;
        public:
        //if the order ever becomes problem move it to deficated init function called from main loop
        //TH_driver(){
        void init(){
            DEBUG(3,"inside TH init");
            _configured++;
            if(_configured==1){
                DEBUG(4,"configuring everything");
                //initialize pio code aquiring data
                init_pio();
                //initialize dma for copying data
                init_dma();
                //start pio
                DEBUG(3,"starting pio");
                pio_sm_set_enabled(pio0, _state_machine, true);
            }
            else
                DEBUG(4,"someone already did config exitting");
            LOG("initialized TH");
        }
        uint16_t get_code(){
            return buffer[TH_channel];
        }
        float get_code_averaged(){
            uint32_t sum=0;
            for(unsigned int i=TH_channel;i<LOOP_LENGTH;i+=2)
                sum+=buffer[i];
            return sum/(LOOP_LENGTH/2.0f);
        }
        float get_voltage()
        {return code_to_V(get_code());}
        float get_resistance()
        {return V_to_R(code_to_V(get_code()),config_file.TH[TH_channel].gain,config_file.TH[TH_channel].offset);}
        float get_temp_fast()
        {return code_to_temp(get_code(),*const_cast<TH_params*>(&config_file.TH[TH_channel]));}
        float get_temp()
        {return code_to_temp(get_code_averaged(),*const_cast<TH_params*>(&config_file.TH[TH_channel]));}
        float estimate_std(){
            float variance=0.0f;
            float mean=get_code_averaged();
            for(unsigned int i=TH_channel;i<LOOP_LENGTH;i+=2){
                variance+=(buffer[i]-mean)*(buffer[i]-mean);
            }
            return std::sqrt(variance/(LOOP_LENGTH/2))*(code_to_temp(mean-1,*const_cast<TH_params*>(&config_file.TH[TH_channel]))-code_to_temp(mean,*const_cast<TH_params*>(&config_file.TH[TH_channel])));
        }
        private:
        void init_pio(){
            DEBUG(3,"initializing TH pio");
            uint offset = pio_add_program(pio0, &th_adc_comunication_program);
            printf("Loaded program at %d\n", offset);
            pio_gpio_init(pio0, CLK);
            pio_gpio_init(pio0, DIN);
            pio_gpio_init(pio0, TRIGGER);
            
            pio_sm_set_consecutive_pindirs(pio0, _state_machine, CLK, 1, true);
            pio_sm_set_consecutive_pindirs(pio0, _state_machine, DIN, 1, false);
            pio_sm_set_consecutive_pindirs(pio0, _state_machine, TRIGGER, 1, true);
            //clk config
            pio_sm_config config=th_adc_comunication_program_get_default_config(offset);
            sm_config_set_clkdiv(&config,CLK_DIVIDER);
            sm_config_set_set_pins(&config,CLK,1);
            sm_config_set_in_pins(&config,DIN);
            sm_config_set_sideset_pins(&config,TRIGGER);
            sm_config_set_fifo_join(&config,PIO_FIFO_JOIN_RX);
            sm_config_set_in_shift(&config,false,true,32);
            pio_sm_init(pio0, _state_machine, offset, &config);
            printf("pio configured\n");
            pio_sm_set_enabled(pio0, _state_machine, true);
            printf("pio started\n");
        }
        void init_dma(){
            DEBUG(3,"initializing TH dma");
            //initialize DMA storing data
            dma_channel_claim( main_DMA_channel);
            dma_channel_claim(chain_DMA_channel);
            dma_channel_config config = dma_channel_get_default_config(main_DMA_channel);
            //configure PWM writing buffer to DMA
            channel_config_set_chain_to(&config,chain_DMA_channel);
            channel_config_set_transfer_data_size(&config, DMA_SIZE_32);
            channel_config_set_read_increment(&config, false);
            channel_config_set_write_increment(&config, true);
            channel_config_set_dreq(&config,pio_get_dreq(pio0,_state_machine,false));
            dma_channel_configure(main_DMA_channel,&config,buffer,&pio0->rxf[_state_machine],LOOP_LENGTH/2,false);
            //configure chained restarting DMA
            config = dma_channel_get_default_config(chain_DMA_channel);
            channel_config_set_transfer_data_size(&config, DMA_SIZE_32);
            channel_config_set_read_increment(&config, false);
            channel_config_set_write_increment(&config, false);
            dma_channel_configure(chain_DMA_channel,&config,&dma_hw->ch[main_DMA_channel].al2_write_addr_trig,&restart_ptr,1,true);
        }
    };
}

inline TH_driver<0> TH1;
inline TH_driver<1> TH2;

template<int TH_channel>
TH_driver<TH_channel>* get_correct_channel(){
    static_assert(TH_channel==0 || TH_channel==1, "wrong channel selected in get_correct channel");
    if(TH_channel==0)
        return reinterpret_cast<TH_driver<TH_channel>*>(&TH1);
    else
        return reinterpret_cast<TH_driver<TH_channel>*>(&TH2);
}

#endif //TH_LIB_GUARD