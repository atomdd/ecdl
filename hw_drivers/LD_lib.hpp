#pragma once
#include "pico/stdlib.h"
#include "hardware/spi.h"
#include "VISA_parser/configuration_variable.hpp"
#include <algorithm>
#include <cmath>

namespace{
    spi_inst_t* myspi=spi1;
    const int BAUD=1000*1000; //conservatinve 1MHz
    const int CS  =9;
    const int MOSI=11;
    const int SCLK=10;
    //shunt resistor voltage scaling (5Vref, R-R divider, 2^12 full scale code)
    const float VOLTS_PER_CODE=5.0f/2/(1<<12);
}

class{
    private:
    float last_current=std::nan("");
    bool enabled=true;
    uint16_t current_to_code(float current){
        float tmp=config_file.LD.current_to_voltage*current+config_file.LD.offset;
        tmp=std::clamp(tmp,0.0f,5.0f);
        //12bit code for 5V ref
        tmp*=(1<<12)/5.0f;
        uint16_t code=std::round(tmp);
        //construct header
        code|=0<<15; //channel A
        code|=1<<14; //buffered out
        code|=1<<13; //gain x1
        code|=1<<12; //don't shut down
        return code;
    }
    public:
    //blanking is just for protection LDs from saturating MOS when vcc drops
    bool blanked=false;
    void init_spi(){
        spi_init(myspi,BAUD);
        spi_set_format(myspi,16, SPI_CPOL_0 , SPI_CPHA_0, SPI_MSB_FIRST);
        gpio_set_function(CS, GPIO_FUNC_SPI);
        gpio_set_function(MOSI, GPIO_FUNC_SPI);
        gpio_set_function(SCLK, GPIO_FUNC_SPI);
    }
    void set_current(float current){
        if(current>config_file.LD.max_current){
            printf("you exceeded allowed max current\n");
            current=config_file.LD.max_current;
        }
        last_current=current;
        uint16_t tmp=current_to_code(current);
        if(!enabled || blanked)
            tmp=0;
        spi_write16_blocking(myspi, &tmp, 1);
    }
    float get_current()
    {return last_current;}
    float get_resistor_voltage(){
        if(!enabled)
            return 0.0f;
        return current_to_code(last_current)*VOLTS_PER_CODE;
    }
    //latching disable for LD protection in monitoring
    void disable(){
        enabled=false;
        set_current(0);
    }
} LD_singleton;