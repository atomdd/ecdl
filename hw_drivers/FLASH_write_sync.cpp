#include "FLASH_write_sync.hpp"
#include "pico/stdlib.h"
#include <cstdint>
#include "logging.hpp"

namespace{
    enum class lock_states : uint32_t{
        FREE=0,
        TRY_TO_LOCK=1,
        LOCKED=2,
        FREE_TO_UNLOCK=4
    };
    static volatile inline lock_states lock=lock_states::FREE;
}

void __no_inline_not_in_flash_func(yield_if_needed)(){
    if(lock!=lock_states::FREE){
        DEBUG(2,"yielding\n");
        gpio_put(2,1);
        gpio_put(5,1);
        if(lock==lock_states::TRY_TO_LOCK){
            lock=lock_states::LOCKED;
            while(lock!=lock_states::FREE_TO_UNLOCK)
            {}
            lock=lock_states::FREE;
        }
        else {
            ERROR("impossible state in FLASH write synchronizer yield");
        }
        DEBUG(2,"yield ended\n");
    }
}

void lock_flash(){
    DEBUG(2,"locking\n");
    sleep_us(100);
    if(lock!=lock_states::FREE){
        ERROR("impossible state in FLASH write synchronizer lock (possible double lock)");
    }
    lock=lock_states::TRY_TO_LOCK;
    while(lock!=lock_states::LOCKED)
    {}
    DEBUG(2,"locked\n");
}

void unlock_flash(){
    DEBUG(2,"unlocking\n");
    if(lock!=lock_states::LOCKED){
        ERROR("impossible state in FLASH write synchronizer unlock");
    }
    lock=lock_states::FREE_TO_UNLOCK;
}