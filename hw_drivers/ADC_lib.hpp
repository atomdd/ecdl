#ifndef ADC_LIB_GUARD
#define ADC_LIB_GUARD
#include <stdint.h>
#include "hardware/adc.h"
#include "hardware/dma.h"
#include "logging.hpp"

namespace{
const unsigned int buffer_log_size=6; //64 samples total, 32 on each side
const unsigned int buffer_size=1<<(buffer_log_size);
const float ADC_scaling=3.3*(39+10)/10/(1<<12); //3.3V * (39k+10k)/10k * 4096LSB
}

class ADC_sketch{
    private:
    volatile uint16_t buffer[buffer_size]  __attribute__ ((aligned (buffer_size)));
    public:
    void init(){
        LOG("address of buffer is aligned [0-correct]: ",static_cast<int>(reinterpret_cast<int>(buffer)%buffer_size));
        if(static_cast<int>(reinterpret_cast<int>(buffer)%buffer_size)!=0){
            printf("allocation ERROR!\n");
            while(1){}
        }
        adc_init();
        adc_gpio_init(28);
        adc_gpio_init(29);
        //gpio 28 and 29
        //important. first sample is trash and only then we get 28/29/28/29/28/29/...
        adc_set_round_robin(0xc);
        //slowest possible (around 730Hz)
        adc_set_clkdiv(0xffff);
        //enable fifo - needed for dma
        adc_fifo_setup(true,true,4,true,false);
        //we make single DMA perform 2^32 measurements without looping. In theory it'll stop but it'll happen only after almost 68 days
        dma_channel_claim(0);
        dma_channel_config config = dma_channel_get_default_config(0);
        channel_config_set_transfer_data_size(&config, DMA_SIZE_16);
        channel_config_set_read_increment(&config, false);
        channel_config_set_write_increment(&config, true);
        channel_config_set_dreq(&config,DREQ_ADC);
        channel_config_set_ring(&config, true, buffer_log_size+1); //write addresses, +1 as we're DMAing 2B words
        dma_channel_configure(0,&config,buffer,&adc_hw->fifo,0xffffffff,true);
        //enable adc
        adc_run(true);
        LOG("initialized ADC");
    }
    void print_buffer(){
        for(uint16_t i=0;i<buffer_size;i++)
            printf("%d ",buffer[i]);
        printf("\n");
    }
    float get_VCC(){
        //VCC - gpio 29, actually first in queue
        //TODO: implement some fancy filtering, error (MSB) checking, etc
        return average(0)*ADC_scaling;
    }
    float get_LD(){
        //VCC - gpio 29, actually first in queue
        //TODO: implement some fancy filtering, error (MSB) checking, etc
        return get_VCC()-(average(1)*ADC_scaling);
    }
    float get_MOS(){
        //VCC - gpio 29, actually first in queue
        //TODO: implement some fancy filtering, error (MSB) checking, etc
        return average(1)*ADC_scaling;
    }
    private:
    float average(uint8_t offset){
        float sum=0;
        unsigned int count=0;
        asm ("" ::: "memory");
        for(uint16_t i=offset;i<buffer_size;i+=2){
            if(!(buffer[i]&0x8000)){
                sum+=buffer[i];
                count++;
            }
        }
        DEBUG(4,"averaging count in ADC");
        DEBUG(4,static_cast<int16_t>(count));
        if(count==0){
            printf("ADC summation ERROR\n");
            while(1){}
        }
        return sum/count;
    }
};

inline ADC_sketch ADC_singleton;

#endif //ADC_LIB_GUARD