#ifndef TEC_LIB_GUARD
#define TEC_LIB_GUARD
#include "VISA_parser/configuration_variable.hpp"
#include "pico/stdlib.h"
#include "hardware/pwm.h"
#include "hardware/dma.h"
#include <cmath>
#include "logging.hpp"
#include "ADC_lib.hpp"

namespace{
    const uint8_t TEC_slice[]={3,0};
    const uint8_t TEC_pwm[]  ={6,0};
    const uint8_t TEC_en[]   ={7,1};
    const uint16_t PWM_period=266; //500KHz at 133MHz, has to be even to be able to get 0V
    const uint16_t random_mask[1024]={856, 596, 63, 615, 141, 782, 201, 234, 498, 898, 592, 717, 770, 736, 485, 313, 5, 863, 278, 1009, 14, 111, 456, 1008, 656, 392, 985, 483, 981, 743, 269, 503, 482, 149, 462, 740, 433, 77, 449, 403, 647, 748, 617, 248, 689, 428, 646, 271, 638, 17, 602, 220, 386, 614, 148, 878, 633, 178, 961, 520, 225, 561, 244, 893, 24, 193, 892, 515, 601, 639, 496, 686, 512, 270, 917, 246, 468, 621, 388, 372, 968, 791, 505, 391, 632, 839, 642, 542, 417, 761, 549, 170, 518, 696, 859, 990, 238, 594, 84, 777, 710, 136, 907, 939, 914, 590, 74, 233, 60, 254, 113, 42, 258, 1015, 335, 452, 556, 951, 72, 867, 625, 98, 79, 494, 474, 18, 124, 28, 814, 550, 368, 394, 4, 595, 385, 115, 855, 429, 958, 599, 67, 153, 973, 817, 838, 194, 1020, 657, 291, 781, 974, 548, 684, 658, 71, 323, 936, 824, 198, 536, 766, 132, 834, 675, 183, 832, 572, 668, 93, 608, 379, 808, 788, 583, 207, 353, 255, 659, 916, 600, 622, 694, 289, 430, 444, 651, 728, 606, 721, 508, 29, 707, 465, 677, 742, 20, 875, 701, 652, 531, 145, 889, 70, 731, 862, 156, 46, 603, 860, 709, 408, 189, 995, 1014, 702, 619, 901, 88, 337, 160, 374, 10, 988, 1011, 108, 660, 349, 277, 218, 253, 179, 490, 90, 262, 700, 412, 144, 223, 367, 8, 1003, 768, 334, 247, 316, 661, 792, 693, 957, 879, 41, 940, 431, 756, 214, 342, 543, 734, 493, 975, 216, 497, 217, 948,48, 66, 789, 529, 778, 174, 983, 43, 845, 644, 448, 680, 240, 523, 727, 466, 865, 913, 829, 49, 404, 964, 235, 134, 528, 626, 871, 86, 31, 204, 302, 643, 997, 598, 746, 459, 81, 76, 613, 843, 384, 73, 65, 371, 298, 804, 182, 904, 972, 687, 1023, 389, 36, 567, 938, 126, 720, 779, 121, 671, 517, 478, 870, 94, 327, 984, 30, 99, 180, 664, 373, 899, 481, 123, 816, 841, 142, 637, 794, 969, 329, 419, 627, 929, 683, 554, 55, 97, 155, 825, 301, 210, 32, 607, 47, 786, 673, 365, 85, 436, 346, 672, 895, 7, 857, 586, 356, 877, 192, 200, 143, 650,805, 341, 406, 522, 861, 164, 937, 991, 227, 744, 297, 128, 1006, 577, 173, 568, 445, 999, 425, 453, 612, 750, 597, 241, 432, 538, 221, 243, 21, 826, 690, 393, 711, 351, 847, 236, 146, 416, 547, 53, 118, 890, 749, 526, 434, 739, 706, 130, 256, 169, 376, 987, 765, 414, 910, 573, 147, 102, 80, 854, 472, 181, 265, 131, 172, 175, 44, 811, 495, 375, 82, 363, 458, 724, 263, 630, 691, 402, 757, 224, 137, 307, 304, 215, 516, 582, 303, 579, 676, 161, 610, 519, 670, 275, 463, 797, 628, 753, 737, 257, 159, 713, 135, 649, 1021, 104, 771, 506, 1000, 16, 340, 360, 803, 446, 382, 191, 410, 352, 881, 34, 729, 593, 322, 915, 119, 640, 187, 605, 909, 809, 585, 387, 787, 435, 663, 339, 158, 950, 571, 348, 280, 943, 827, 629, 105, 864, 338, 545, 273, 100, 796, 776,886, 305, 308, 423, 945, 6, 332, 655, 112, 157, 209, 92, 730, 320, 361, 678, 942, 831, 774, 812, 57, 801, 726, 460, 552, 23, 559, 266, 949, 591, 665, 927, 858, 624, 1, 714, 464, 260, 319, 333, 539, 268, 872, 844, 359, 955, 272, 1005, 168, 723, 611, 484, 718, 479, 760, 140, 206, 59, 618, 274, 882, 330, 239, 553, 267, 176, 58, 364, 358, 129, 534, 747, 25, 336, 331, 967, 19, 397, 318, 725, 13, 799, 167, 343, 840, 378, 68, 52, 912,455, 931, 471, 800, 645, 250, 920, 310, 283, 37, 631, 807, 211, 533, 921, 110, 758, 120, 884, 557, 897, 380, 1002, 293, 986, 634, 833, 321, 395, 422, 941, 317, 785, 91, 296, 51, 880, 537, 793, 560, 325, 324, 83, 905, 398, 163, 507, 1017, 851, 56, 947, 944, 900, 286, 979, 242, 1001, 989, 569, 251, 773, 264, 868, 588, 89, 842, 888, 513, 355, 125, 362, 821, 576, 27, 798, 441, 654, 930, 33, 521, 978, 965, 229, 443, 354, 249, 62, 213, 427, 177, 226, 39, 405, 396, 934, 292, 409, 580, 186, 284, 982, 151, 970, 3, 722, 122, 874, 806, 830, 421, 188, 795, 835, 0, 681, 326, 195, 106, 685, 837, 850, 527, 966, 754, 846, 152, 282, 873, 426, 252, 994, 578, 202, 45, 952, 285, 926, 418, 708, 212, 524, 309, 616, 12, 971, 980, 699, 440, 139, 609, 784, 790, 741, 635, 705, 849, 315, 589, 525, 866, 237, 544, 231, 933, 745, 772, 688, 437, 203, 903, 107, 311, 150, 288, 1007, 752, 763, 424, 69, 400, 853, 300, 413, 555, 245, 738, 703, 653, 623, 442, 669, 345, 563, 674, 491, 486, 848,954, 541, 26, 103, 662, 165, 469, 22, 502, 499, 712, 347, 439, 461, 411, 935, 783, 959, 489, 127, 735, 764,532, 819, 695, 96, 852, 719, 473, 587, 775, 570, 1018, 399, 906, 261, 977, 477, 50, 2, 447, 40, 228, 704, 314, 620, 197, 61, 530, 562, 960, 9, 480, 78, 566, 953, 918, 407, 162, 232, 64, 470, 894, 415, 171, 902, 500,815, 196, 509, 487, 514, 287, 205, 87, 54, 891, 780, 15, 584, 281, 813, 420, 993, 1022, 715, 923, 896, 564,1016, 932, 383, 295, 946, 732, 511, 733, 924, 922, 1013, 535, 928, 823, 769, 641, 401, 101, 836, 185, 992, 679, 666, 276, 116, 828, 581, 475, 667, 451, 546, 887, 219, 963, 682, 822, 457, 75, 716, 344, 558, 312, 369,810, 751, 133, 350, 328, 698, 925, 199, 299, 510, 166, 377, 976, 802, 1010, 208, 998, 230, 184, 876, 450, 755, 604, 575, 648, 636, 762, 454, 306, 114, 190, 883, 11, 279, 869, 476, 767, 911, 467, 492, 366, 38, 138, 438, 574, 154, 1019, 820, 885, 1012, 357, 565, 504, 109, 95, 488, 290, 370, 540, 697, 501, 919, 956, 818, 35,259, 692, 962, 222, 908, 381, 996, 294, 551, 117, 390, 759, 1004};
}

namespace{
//TODO: fix power vs voltage thing
template<int TEC_channel>
class TEC_driver{
    private:
    float _last_power=std::nan("");
    const uint8_t PWM_DMA_channel=2*TEC_channel+1;
    const uint8_t chained_DMA_channel=2*TEC_channel+2;
    static const uint16_t PWM_buffer_len=1024; //it is critical that this is power of 2
    uint16_t PWM_buffer[PWM_buffer_len] __attribute__ ((aligned (4))); //at 500kHz repetition rate it's ~2ms, we only use 2 lower bytes
    const uint32_t _for_chained_dma=reinterpret_cast<uint32_t>(&PWM_buffer[0]);
    public:
    void init()
    { 
        // Tell GPIO it is allocated to the PWM
        gpio_init(TEC_pwm[TEC_channel]);
        gpio_init(TEC_en[TEC_channel]);
        gpio_set_function(TEC_pwm[TEC_channel], GPIO_FUNC_PWM);
        gpio_set_function(TEC_en[TEC_channel], GPIO_FUNC_SIO);
        //disable TEC driver
        gpio_put(TEC_en[TEC_channel],0);
        //set to output
        gpio_set_dir(TEC_en[TEC_channel],true);
        // divider 1
        pwm_set_clkdiv_int_frac(TEC_slice[TEC_channel],1,0);
        pwm_set_wrap(TEC_slice[TEC_channel], PWM_period-1);
        pwm_set_chan_level(TEC_slice[TEC_channel], PWM_CHAN_A, PWM_period/2);
        pwm_set_enabled(TEC_slice[TEC_channel], true);
        
        //fill buffer with 0V PWM
        static_assert(PWM_period%2==0, "odd PWM period currently not supported");
        for(uint16_t i=0;i<PWM_buffer_len;i++)
            PWM_buffer[i]=PWM_period/2;
        //configure PWM writing buffer to DMA
        dma_channel_claim(PWM_DMA_channel);
        dma_channel_config config = dma_channel_get_default_config(PWM_DMA_channel);
        channel_config_set_chain_to(&config,chained_DMA_channel);
        channel_config_set_transfer_data_size(&config, DMA_SIZE_16);
        channel_config_set_read_increment(&config, true);
        channel_config_set_write_increment(&config, false);
        channel_config_set_dreq(&config,DREQ_PWM_WRAP0+TEC_slice[TEC_channel]);
        dma_channel_configure(PWM_DMA_channel,&config,&pwm_hw->slice[TEC_slice[TEC_channel]].cc,PWM_buffer,PWM_buffer_len,false); //lower two bytes of cc are channel A
        //configure chained restarting DMA
        config = dma_channel_get_default_config(chained_DMA_channel);
        dma_channel_claim(chained_DMA_channel);
        channel_config_set_transfer_data_size(&config, DMA_SIZE_32);
        channel_config_set_read_increment(&config, false);
        channel_config_set_write_increment(&config, false);
        dma_channel_configure(chained_DMA_channel,&config,&dma_hw->ch[PWM_DMA_channel].al3_read_addr_trig,&_for_chained_dma,1,true); //lower two bytes of cc are channel A

        LOG("initialized TEC:",TEC_channel);
    }
    void enable()
    {gpio_put(TEC_en[TEC_channel],1);}
    void disable()
    {gpio_put(TEC_en[TEC_channel],0);}
    void set_voltage(float wanted,float Vcc=std::nan("")){
        //we need to read ADC directly
        if(std::isnan(Vcc)){
            Vcc=ADC_singleton.get_VCC();
        }
        float max_voltage=config_file.TEC[TEC_channel].max_voltage;
        //clamp to max_voltage
        if(fabs(wanted)>fabs(max_voltage)){
            wanted=copysignf(max_voltage,wanted);
        }
        //clamp to Vcc
        if(fabs(wanted)>fabs(Vcc)){
            wanted=copysignf(Vcc,wanted);
        }
        //stop cooling
        if(config_file.TEC[TEC_channel].heating==1){
            if(wanted<0.0f)
                wanted=0.0f;
        }
        if(config_file.TEC[TEC_channel].invert==1)
            wanted*=-1.0f;
        _last_power=wanted*wanted/config_file.TEC[TEC_channel].resistance;
        int32_t pwm_value=(PWM_period*PWM_buffer_len/2)*(1+wanted/Vcc);
        uint16_t major=pwm_value/PWM_buffer_len;
        uint16_t minor=pwm_value-major*PWM_buffer_len;
        // LOG("PWM value: ",major);
        // LOG("PWM reminder: ",minor);
        //set major
        for(uint16_t i=0;i<PWM_buffer_len;i++)
            PWM_buffer[i]=major;
        DEBUG(3,"major set");
        //set minor
        for(uint16_t i=0;i<minor;i++)
            PWM_buffer[random_mask[i]]+=1;
        DEBUG(3,"correction done");
    }
    
    void set_power(float wanted,float Vcc=std::nan("")){
        float voltage=std::sqrt(wanted*config_file.TEC[TEC_channel].resistance);
        set_voltage(voltage,Vcc);
    }
    float get_power(){
        return _last_power;
    }

    //to be called after both inits to sync channels
    static void sync_channels()
    {
        pwm_set_mask_enabled(0);
        pwm_set_counter(TEC_slice[0],0);
        pwm_set_counter(TEC_slice[1],PWM_period/2);
        pwm_set_mask_enabled((1<<TEC_slice[0])|(1<<TEC_slice[1]));

    }
};
}

inline TEC_driver<0> TEC1;
inline TEC_driver<1> TEC2;

template<int TEC_channel>
TEC_driver<TEC_channel>* get_correct_TEC(){
    static_assert(TEC_channel==0 || TEC_channel==1, "wrong channel selected in get_correct channel");
    if(TEC_channel==0)
        return reinterpret_cast<TEC_driver<TEC_channel>*>(&TEC1);
    else
        return reinterpret_cast<TEC_driver<TEC_channel>*>(&TEC2);
}

#endif // TEC_LIB_GUARD