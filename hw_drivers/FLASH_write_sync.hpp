#pragma once

void yield_if_needed();
void lock_flash();
void unlock_flash();